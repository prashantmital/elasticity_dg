/*
This code is licensed under the "GNU GPL version 2 or later". See
license.txt or https://www.gnu.org/licenses/gpl-2.0.html

Copyright 2012-2014: Thomas Wick
*/

// This program (is based on deal.II step-30)
// Phase-field crack propagation in elasticity
// The specific example is the single edge notched tension
// test (see Miehe et al. 2010a,b)

// A TODO list can be found at the end

#include <base/quadrature_lib.h>
#include <base/function.h>
#include <base/tensor_function.h>
#include <base/utilities.h>
            
#include <lac/vector.h>
#include <lac/sparse_matrix.h>
#include <lac/constraint_matrix.h>
#include <lac/block_sparse_matrix.h>
#include <lac/sparse_direct.h>

#include <grid/tria.h>
#include <grid/grid_generator.h>
#include <grid/grid_out.h>
#include <grid/grid_refinement.h>
#include <grid/tria_accessor.h>
#include <grid/tria_iterator.h>
#include <grid/tria_boundary_lib.h>
#include <grid/grid_in.h>
#include <grid/grid_tools.h>

#include <fe/fe_values.h>
#include <fe/fe_system.h>
#include <fe/mapping_q1.h>
#include <fe/fe_dgq.h>
#include <fe/fe_dgp.h>
#include <fe/fe_q.h>

#include <dofs/dof_handler.h>
#include <dofs/dof_accessor.h>
#include <dofs/dof_tools.h>
#include <dofs/dof_renumbering.h>

#include <numerics/data_out.h>
#include <numerics/solution_transfer.h>
#include <numerics/vector_tools.h>
#include <numerics/matrix_tools.h>

#include <base/timer.h>

#include <iostream>
#include <fstream>
#include <algorithm>

#include "input_handler.h"

using namespace dealii;

//-----------------------------------------------------------------------------
// Enumeration to specify which case to run
enum class TestCase {
    none,
    benchmark,
    cook
};

// Global access of solid parameters
class lameconstants {
public:
  static double get_lame_mu() { return mu; }
  static double get_lame_lambda() { return lambda; }
  void set_lame_mu(double mu_inp) { mu = mu_inp; }
  void set_lame_lambda(double lambda_inp) { lambda = lambda_inp; }

private:
  static double mu;
  static double lambda;
};

double lameconstants::mu = 0.0;
double lameconstants::lambda = 0.0;

// Define some tensors for cleaner notation later.
namespace Tensors
{

  template <int dim>
  inline Tensor<2, dim>
  get_grad_u (
    unsigned int q,
    const std::vector<std::vector<Tensor<1, dim> > > &old_solution_grads)
  {
    Tensor<2, dim> structure_continuation;
    structure_continuation[0][0] = old_solution_grads[q][0][0];
    structure_continuation[0][1] = old_solution_grads[q][0][1];
    structure_continuation[1][0] = old_solution_grads[q][1][0];
    structure_continuation[1][1] = old_solution_grads[q][1][1];

    return structure_continuation;
  }

  template <int dim>
  inline Tensor<2, dim>
  get_Identity ()
  {
    Tensor<2, dim> identity;
    identity[0][0] = 1.0;
    identity[0][1] = 0.0;
    identity[1][0] = 0.0;
    identity[1][1] = 1.0;

    return identity;
  }

  template <int dim>
  inline Tensor<1, dim>
  get_u (
    unsigned int q,
    const std::vector<Vector<double> > &old_solution_values)
  {
    Tensor<1, dim> u;
    u[0] = old_solution_values[q](0);
    u[1] = old_solution_values[q](1);

    return u;
  }
}

//-----------------------------------------------------------------------------
// SVD Function
template <int dim>
void eigen_vectors_and_values(
  double &E_eigenvalue_1, double &E_eigenvalue_2,
  Tensor<2,dim> &ev_matrix,
  const Tensor<2,dim> &matrix)
{

  // Compute eigenvectors
  Tensor<1,dim> E_eigenvector_1;
  Tensor<1,dim> E_eigenvector_2;
  if (std::abs(matrix[0][1]) < 1e-10*std::abs(matrix[0][0])
      || std::abs(matrix[0][1]) < 1e-10*std::abs(matrix[1][1]))
    {
      // E is close to diagonal
      E_eigenvalue_1 = matrix[0][0];
      E_eigenvector_1[0]=1;
      E_eigenvector_1[1]=0;
      E_eigenvalue_2 = matrix[1][1];
      E_eigenvector_2[0]=0;
      E_eigenvector_2[1]=1;
    }
  else
    {
      double sq = std::sqrt((matrix[0][0] - matrix[1][1]) * (matrix[0][0] - matrix[1][1]) + 4.0*matrix[0][1]*matrix[1][0]);
      E_eigenvalue_1 = 0.5 * ((matrix[0][0] + matrix[1][1]) + sq);
      E_eigenvalue_2 = 0.5 * ((matrix[0][0] + matrix[1][1]) - sq);

      E_eigenvector_1[0] = 1.0/(std::sqrt(1 + (E_eigenvalue_1 - matrix[0][0])/matrix[0][1] * (E_eigenvalue_1 - matrix[0][0])/matrix[0][1]));
      E_eigenvector_1[1] = (E_eigenvalue_1 - matrix[0][0])/(matrix[0][1] * (std::sqrt(1 + (E_eigenvalue_1 - matrix[0][0])/matrix[0][1] * (E_eigenvalue_1 - matrix[0][0])/matrix[0][1])));
      E_eigenvector_2[0] = 1.0/(std::sqrt(1 + (E_eigenvalue_2 - matrix[0][0])/matrix[0][1] * (E_eigenvalue_2 - matrix[0][0])/matrix[0][1]));
      E_eigenvector_2[1] = (E_eigenvalue_2 - matrix[0][0])/(matrix[0][1] * (std::sqrt(1 + (E_eigenvalue_2 - matrix[0][0])/matrix[0][1] * (E_eigenvalue_2 - matrix[0][0])/matrix[0][1])));
    }

  ev_matrix[0][0] = E_eigenvector_1[0];
  ev_matrix[0][1] = E_eigenvector_2[0];
  ev_matrix[1][0] = E_eigenvector_1[1];
  ev_matrix[1][1] = E_eigenvector_2[1];

  // Sanity check if orthogonal
  double scalar_prod = 1.0e+10;
  scalar_prod = E_eigenvector_1[0] * E_eigenvector_2[0] + E_eigenvector_1[1] * E_eigenvector_2[1];

  if (scalar_prod > 1.0e-6)
    {
      std::cout << "Seems not to be orthogonal" << std::endl;
      abort();
    }
}

template<int dim>
double tensor_norm(const Tensor<2,dim> &matrix)
{
  double ev1=0.0, ev2=0.0;
  Tensor<2,dim> ev_matrix;
  eigen_vectors_and_values(ev1, ev2, ev_matrix, matrix);
  return std::max(std::fabs(ev1), std::fabs(ev2));
}

template<int dim>
Tensor<2,dim> deviatoric_stress(const Tensor<2,dim> &stress)
{
  double hydrostatic_stress = 0.0;
  for (unsigned int i=0; i<dim; ++i)
    hydrostatic_stress += stress[i][i]/dim;
  Tensor<2,dim> deviatoric_stress;
  deviatoric_stress = stress;
  for (unsigned int i=0; i<dim; ++i)
    deviatoric_stress[i][i] -= hydrostatic_stress;
  return deviatoric_stress;
}

template<int dim>
class BenchmarkDirichletBC : public TensorFunction<1,dim>
{
  public:
  BenchmarkDirichletBC() : TensorFunction<1,dim> () {}
  virtual ~BenchmarkDirichletBC () { /*~TensorFunction<1,dim> (); */ }

  virtual Tensor<1,dim> value (const Point<dim> &p) const 
  {  
    Tensor<1,dim> return_tensor;
    return_tensor = 0;
    return return_tensor;
  }

  virtual void value_list (const std::vector<Point<dim>> &points,
      std::vector<Tensor<1,dim>> &values) const {
    Assert(points.size()==values.size(),
        ExcDimensionMismatch(values.size(),points.size()));
    for (unsigned int i=0; i<points.size(); ++i)
      values[i] = value(points[i]);
  }
};

template<int dim>
class CookDirichletBC : public TensorFunction<1,dim>
{
  public:
  CookDirichletBC() : TensorFunction<1,dim> () {}
  virtual ~CookDirichletBC () { /*~TensorFunction<1,dim> (); */ }

  virtual Tensor<1,dim> value (const Point<dim> &p) const 
  {  
    Tensor<1,dim> return_tensor;
  
    // left: u=(0, 0)
    if (std::fabs(p(0)-0)<=1e-8 && (p(1)>=0 && p(1)<=44))
      return_tensor = 0;
    else
      return_tensor = 0;

    return return_tensor;
  }

  virtual void value_list (const std::vector<Point<dim>> &points,
      std::vector<Tensor<1,dim>> &values) const {
    Assert(points.size()==values.size(),
        ExcDimensionMismatch(values.size(),points.size()));
    for (unsigned int i=0; i<points.size(); ++i)
      values[i] = value(points[i]);
  }
};


template<int dim>
class CookNeumannBC : public TensorFunction<1,dim>
{
  public:
  CookNeumannBC() : TensorFunction<1,dim> () {}
  virtual ~CookNeumannBC () { /*~TensorFunction<1,dim> (); */ }

  virtual Tensor<1,dim> value (const Point<dim> &p) const 
  {  
    Tensor<1,dim> return_tensor;
  
    // left: u=(0, 0)
    if (std::fabs(p(0)-48)<=1e-8 && (p(1)>=44 && p(1)<=60))
    {
      return_tensor[0] = 0;
      return_tensor[1] = 6.25;
    }
    else
      return_tensor = 0;

    return return_tensor;
  }

  virtual void value_list (const std::vector<Point<dim>> &points,
      std::vector<Tensor<1,dim>> &values) const {
    Assert(points.size()==values.size(),
        ExcDimensionMismatch(values.size(),points.size()));
    for (unsigned int i=0; i<points.size(); ++i)
      values[i] = value(points[i]);
  }
};


template<int dim>
class BenchmarkForcingFN : public TensorFunction<1,dim>
{
  public:
  BenchmarkForcingFN(const double lame_mu, 
        const double lame_lambda) : 
        TensorFunction<1,dim> (), 
        mu(lame_mu), 
        lambda(lame_lambda) 
        {
          if (mu!=1)
          {
            std::cout << "Benchmark MUST have mu=1.0!\n";
            abort();
          }
        }

  virtual ~BenchmarkForcingFN () { /*~TensorFunction<1,dim> (); */ }

  virtual Tensor<1,dim> value (const Point<dim> &p) const 
  {  
    Tensor<1,dim> return_tensor;

    const double pi = std::acos(-1);
    const double tx = pi * p(0); //theta_x
    const double ty = pi * p(1); //theta_y

    return_tensor[0] = pi * pi * (
        4*std::sin(2*ty)*(-1+2*std::cos(2*tx))
      - std::cos(tx+ty)
      + (2/(1+lambda))*std::sin(tx)*std::sin(ty)
      );

    return_tensor[1] = pi * pi * (
        4*std::sin(2*tx)*(1-2*std::cos(2*ty))
      - std::cos(tx+ty)
      + (2/(1+lambda))*std::sin(tx)*std::sin(ty)
      );

    return return_tensor;
  }

  virtual void value_list (const std::vector<Point<dim>> &points,
      std::vector<Tensor<1,dim>> &values) const {
    Assert(points.size()==values.size(),
        ExcDimensionMismatch(values.size(),points.size()));
    for (unsigned int i=0; i<points.size(); ++i)
      values[i] = value(points[i]);
  }

private:
  double mu, lambda;
};


template<int dim>
class ZeroTensor : public TensorFunction<1,dim>
{
  public:
  ZeroTensor() : 
        TensorFunction<1,dim> ()
        {}

  virtual ~ZeroTensor () { /*~TensorFunction<1,dim> (); */ }

  virtual Tensor<1,dim> value (const Point<dim> &p) const 
  {  
    Tensor<1,dim> return_tensor;
    return_tensor[0] = 0.0;
    return_tensor[1] = 0.0;
    return return_tensor;
  }

  virtual void value_list (const std::vector<Point<dim>> &points,
      std::vector<Tensor<1,dim>> &values) const {
    Assert(points.size()==values.size(),
        ExcDimensionMismatch(values.size(),points.size()));
    for (unsigned int i=0; i<points.size(); ++i)
      values[i] = value(points[i]);
  }

private:
  double mu, lambda;
};


template <int dim>
class BenchmarkSolutionEG : public TensorFunction<1,dim>
{

public:
  BenchmarkSolutionEG () : TensorFunction<1,dim>(dim) {}
  virtual ~BenchmarkSolutionEG() { }
  
  virtual Tensor<1,dim> value (const Point<dim> &p) const 
  {  
    Tensor<1,dim> return_tensor;

    lameconstants getval;
    const double lambda = getval.get_lame_lambda();
    const double pi = std::acos(-1);
    const double tx = pi * p(0); //theta_x
    const double ty = pi * p(1); //theta_y

    return_tensor[0] = 1.0 * (
        std::sin(2*ty)*(-1+std::cos(2*tx))
      + (1/(1+lambda))*std::sin(tx)*std::sin(ty)
      );

    return_tensor[1] = 1.0 * (
        std::sin(2*tx)*(1-std::cos(2*ty))
      + (1/(1+lambda))*std::sin(tx)*std::sin(ty)
      );

    return return_tensor;
  }

  virtual void value_list (const std::vector<Point<dim>> &points,
      std::vector<Tensor<1,dim>> &values) const {
    Assert(points.size()==values.size(),
        ExcDimensionMismatch(values.size(),points.size()));
    for (unsigned int i=0; i<points.size(); ++i)
      values[i] = value(points[i]);
  }

  virtual Tensor<2,dim> gradient (const Point<dim> &p) const
  {
    Tensor<2,dim> return_tensor;

    lameconstants getval;
    const double lambda = getval.get_lame_lambda();    
    const double pi = std::acos(-1);
    const double tx = pi * p(0); //theta_x
    const double ty = pi * p(1); //theta_y

    return_tensor[0][0] = (
        -2*pi*std::sin(2*ty)*std::sin(2*tx)
      + (pi/(lambda+1))*std::cos(tx)*std::sin(ty)
      );
    return_tensor[0][1] = (
        2*pi*std::cos(2*ty)*(-1+std::cos(2*tx))
      + (pi/(lambda+1))*std::sin(tx)*std::cos(ty)
      );
    return_tensor[1][0] = (
        2*pi*std::cos(2*tx)*(1-std::cos(2*ty))
      + (pi/(lambda+1))*std::cos(tx)*std::sin(ty)
      );
    return_tensor[1][1] = (
        2*pi*std::sin(2*ty)*std::sin(2*tx)
      + (pi/(lambda+1))*std::cos(ty)*std::sin(tx)
      );

    return return_tensor;
  }

  virtual void gradient_list(const std::vector<Point<dim>> &points, 
      std::vector<Tensor<2,dim>> &gradients) const
  {
    Assert(points.size()==gradients.size(),
        ExcDimensionMismatch(gradients.size(),points.size()));
    for (unsigned int i=0; i<points.size(); ++i)
      gradients[i] = gradient(points[i]);    
  }
};



//-----------------------------------------------------------------------------
// Class to perform local cell/face-level computations
template <int dim>
class DGElasticityPhaseFieldEquations
{
public:
    DGElasticityPhaseFieldEquations(InputHandler &parser, int pol_degree);

    void set_min_cell_diameter(const double set_min_cell_diameter)
    {
        min_cell_diameter = set_min_cell_diameter;
    }

    double get_min_cell_diameter()
    {
        return min_cell_diameter;
    }

    void get_solution_data(const BlockVector<double> gsolution)
    {
        solution = gsolution;
    }

    void assemble_cell_term_matrix(const FEValues<dim>& fe_v,
            const double cell_dia,
            FullMatrix<double> &ui_phi_i_matrix) const;

    void assemble_boundary_term_matrix(const FEFaceValues<dim>& fe_v,
            const double face_measure,
            FullMatrix<double> &ui_phi_i_matrix,
            unsigned int boundary_color) const;

    void assemble_face_term_matrix(const FEFaceValuesBase<dim>& fe_v,
            const FEFaceValuesBase<dim>& fe_v_neighbor,
            const double face_measure,
            FullMatrix<double> &ui_phi_i_matrix,
            FullMatrix<double> &ue_phi_i_matrix,
            FullMatrix<double> &ui_phi_e_matrix,
            FullMatrix<double> &ue_phi_e_matrix) const;

    void assemble_cell_term_rhs(const FEValues<dim>& fe_v,
            const double cell_dia,
            Vector<double> &cell_vector) const;

    void assemble_boundary_term_rhs(const FEFaceValues<dim>& fe_v,
            const double face_measure,
            Vector<double> &cell_vector,
            unsigned int boundary_color) const;

    void assemble_face_term_rhs(const FEFaceValuesBase<dim>& fe_v,
            const FEFaceValuesBase<dim>& fe_v_neighbor,
            const double face_measure,
            Vector<double> &ui_phi_i_rhs,
            Vector<double> &ue_phi_i_rhs,
            Vector<double> &ui_phi_e_rhs,
            Vector<double> &ue_phi_e_rhs) const;

    double lame_coefficient_mu_global() { return lame_coefficient_mu; }
    double lame_coefficient_lambda_global() { return lame_coefficient_lambda; }

private:
    InputHandler &parser;
    int degree;
    BlockVector<double> solution;
    double theta_DG;
    double lame_coefficient_mu, lame_coefficient_lambda;
    double penalty_u_bc, penalty_u_face;
    double min_cell_diameter;
    TestCase testcase;
};

//-----------------------------------------------------------------------------
// Constructor: populate parameters from InputHandler class object reference
template <int dim>
DGElasticityPhaseFieldEquations<dim>::
DGElasticityPhaseFieldEquations (InputHandler &parser, int pol_degree) :
    parser(parser), degree(pol_degree)
{
    // Populate parameters
    penalty_u_bc = parser.get_penaltyBC();      // Penalty for Dirichlet BC
    penalty_u_face  = parser.get_penaltyDG();   // Penalty for internal faces
    theta_DG = parser.get_sform();              // DG method parameter
    lame_coefficient_mu = parser.get_lamemu();
    lame_coefficient_lambda =  parser.get_lamelambda();

    // Determine which case to run and store
    if (parser.get_testcase() == "benchmark")
        testcase = TestCase::benchmark;
    else if (parser.get_testcase() == "cook")
        testcase = TestCase::cook;
    else
        testcase = TestCase::none;
}

template <int dim>
void DGElasticityPhaseFieldEquations<dim>::assemble_cell_term_matrix(const FEValues<dim> &fe_values,
        const double cell_dia,
        FullMatrix<double> &ui_phi_i_matrix) const
{
    const unsigned int   dofs_per_cell   = fe_values.dofs_per_cell;
    const unsigned int   n_q_points = fe_values.n_quadrature_points;
    const std::vector<double> &JxW = fe_values.get_JxW_values ();
    const FEValuesExtractors::Vector displacements (0);

    Tensor<2,dim> identity = Tensors::get_Identity<dim> ();

    for (unsigned int q=0; q<n_q_points; ++q)
    {
        std::vector<Tensor<1,dim> > phi_i_u (dofs_per_cell);
        std::vector<Tensor<2,dim> > phi_i_grads_u(dofs_per_cell);
        std::vector<Tensor<2,dim>>  E_phi (dofs_per_cell);
        std::vector<Tensor<2,dim>>  sigma_phi(dofs_per_cell);

        // Compute and store desired quantities
        for (unsigned int k=0; k<dofs_per_cell; ++k)
        {
            phi_i_grads_u[k]    = fe_values[displacements].gradient (k, q);
            E_phi[k] = 0.5 * (phi_i_grads_u[k] + transpose(phi_i_grads_u[k]));
            sigma_phi[k] = 2.0 * lame_coefficient_mu * E_phi[k]
                    + lame_coefficient_lambda * trace(E_phi[k]) * identity;
        }

        for (unsigned int i=0; i<dofs_per_cell; ++i)
        {
            for (unsigned int j=0; j<dofs_per_cell; ++j)
            { // Displacements
              ui_phi_i_matrix(j,i) += JxW[q] * scalar_product(sigma_phi[i], E_phi[j]);
            }
        }
    } // end q_point
}

template <int dim>
void DGElasticityPhaseFieldEquations<dim>::assemble_cell_term_rhs(const FEValues<dim> &fe_values,
        const double cell_dia,
        Vector<double> &local_rhs) const
{
    // TODO
    //    DONE add body force term
    //     add traction term

    const unsigned int   dofs_per_cell   = fe_values.dofs_per_cell;
    const unsigned int   n_q_points = fe_values.n_quadrature_points;
    const std::vector<double> &JxW = fe_values.get_JxW_values ();
    const FEValuesExtractors::Vector displacements (0);

    std::vector<Vector<double> > old_solution_values (n_q_points,
            Vector<double>(dim));
    std::vector<std::vector<Tensor<1,dim> > > old_solution_grads (n_q_points,
            std::vector<Tensor<1,dim> > (dim));
    fe_values.get_function_values (solution, old_solution_values);
    fe_values.get_function_grads (solution, old_solution_grads);

    const std::vector<Point<dim>> &qpoints = fe_values.get_quadrature_points();
    std::vector<Tensor<1,dim>> structure_force(n_q_points);

    if (testcase == TestCase::benchmark)
    {
        BenchmarkForcingFN<dim> body_force(lame_coefficient_mu, lame_coefficient_lambda);
        body_force.value_list(qpoints, structure_force);
    }
    else if (testcase == TestCase::cook) 
    {
        ZeroTensor<dim> body_force;
        body_force.value_list(qpoints, structure_force);
    }
    Tensor<2,dim> identity = Tensors::get_Identity<dim> ();

    for (unsigned int q=0; q<n_q_points; ++q)
    {   // Old Newton iteration data
        const Tensor<2,dim> grad_u = Tensors::get_grad_u<dim>(q, old_solution_grads);
        const Tensor<2,dim> E_u = 0.5 * (grad_u + transpose(grad_u));
        const Tensor<2,dim> sigma_ui = 2.0 * lame_coefficient_mu * E_u + lame_coefficient_lambda * trace(E_u) * identity;
        
        for (unsigned int i=0; i<dofs_per_cell; ++i)
        {   // Test function data
            const Tensor<1,dim> phi_i_u = fe_values[displacements].value(i,q);
            const Tensor<2,dim> phi_i_grads_u = fe_values[displacements].gradient(i,q);
            const Tensor<2,dim> E_phi = 0.5 * (phi_i_grads_u + transpose(phi_i_grads_u));
            // Displacement
            local_rhs(i) -= JxW[q] * (scalar_product(sigma_ui, E_phi)
                                      - structure_force[q] * phi_i_u);
        } // end i
    } // end q_point
}

template <int dim>
void DGElasticityPhaseFieldEquations<dim>::assemble_boundary_term_matrix(const FEFaceValues<dim>& fe_values,
        const double face_measure,
        FullMatrix<double> &ui_phi_i_matrix,
        unsigned int boundary_color) const
{   
    if (testcase==TestCase::cook)
        if (boundary_color != 0)
            return;

    const FEValuesExtractors::Vector displacements (0);
    const unsigned int   dofs_per_cell   = fe_values.dofs_per_cell;
    const unsigned int n_face_q_points   = fe_values.n_quadrature_points;

    Tensor<2,dim> identity = Tensors::get_Identity<dim>();

    for (unsigned int q=0; q<n_face_q_points; ++q)
    {
        const double dx = fe_values.JxW(q);
        const Point<dim>& n = fe_values.normal_vector(q);
        const double penalty = (penalty_u_bc*degree*degree) / face_measure;

        std::vector<Tensor<1,dim> > phi_i_u (dofs_per_cell);
        std::vector<Tensor<2,dim> > phi_i_grads_u(dofs_per_cell);
        std::vector<Tensor<2,dim> > E_LinU(dofs_per_cell);
        std::vector<Tensor<2,dim> > sigma_phi(dofs_per_cell);

        for (unsigned int k=0; k<dofs_per_cell; ++k)
        {
            phi_i_u[k]       = fe_values[displacements].value (k, q);
            phi_i_grads_u[k] = fe_values[displacements].gradient (k, q);
            E_LinU[k] = 0.5 * (phi_i_grads_u[k] + transpose(phi_i_grads_u[k]));
            sigma_phi[k] = 2.0 * lame_coefficient_mu * E_LinU[k] +
                        lame_coefficient_lambda * trace(E_LinU[k]) * identity;
        }

        for (unsigned int i=0;i<dofs_per_cell;++i)
            for (unsigned int j=0;j<dofs_per_cell;++j)
                ui_phi_i_matrix(j,i) += dx *
                     ( phi_i_u[i] * penalty * phi_i_u[j]
                     - (sigma_phi[i] * n) * phi_i_u[j]
                     + theta_DG * (sigma_phi[j] * n) * phi_i_u[i]
                     );
    } // end q_point
}

template <int dim>
void DGElasticityPhaseFieldEquations<dim>::assemble_boundary_term_rhs(const FEFaceValues<dim>& fe_values,
        const double face_measure,
        Vector<double> &local_rhs,
        unsigned int boundary_color) const
{
    if (testcase==TestCase::cook)
        if ((boundary_color!=0) && (boundary_color!=1))
            return;

    const FEValuesExtractors::Vector displacements (0);
    const unsigned int   dofs_per_cell   = fe_values.dofs_per_cell;
    const unsigned int n_face_q_points   = fe_values.n_quadrature_points;
    const std::vector<Point<dim>> &qpoints = fe_values.get_quadrature_points();

    std::vector<Vector<double> > old_solution_values (n_face_q_points,
            Vector<double>(dim));
    std::vector<std::vector<Tensor<1,dim> > > old_solution_grads (n_face_q_points,
            std::vector<Tensor<1,dim> > (dim));
    fe_values.get_function_values (solution, old_solution_values);
    fe_values.get_function_grads (solution, old_solution_grads);

    std::vector<Tensor<1,dim>> boundary_value(n_face_q_points);
    std::vector<Tensor<1,dim>> neumann_value(n_face_q_points);
    
    if (testcase == TestCase::benchmark)
    {
        BenchmarkDirichletBC<dim> dbc;
        dbc.value_list(qpoints, boundary_value);      
    }
    else
    {
        CookDirichletBC<dim> dbc;
        dbc.value_list(qpoints, boundary_value);
        CookNeumannBC<dim> nbc;
        nbc.value_list(qpoints, neumann_value);
    }

    Tensor<2,dim> identity = Tensors::get_Identity<dim>();

    for (unsigned int q=0; q<n_face_q_points; ++q)
    {
        const double dx = fe_values.JxW(q);
        const Point<dim>& n = fe_values.normal_vector(q);
        const double penalty = (penalty_u_bc*degree*degree) / face_measure;

        Tensor<1,dim> u = Tensors::get_u<dim>(q, old_solution_values);
        Tensor<2,dim> grad_u = Tensors::get_grad_u<dim>(q, old_solution_grads);
        Tensor<2,dim> E = 0.5 * (grad_u + transpose(grad_u));
        Tensor<2,dim> sigma_u = 2.0 * lame_coefficient_mu * E +
                lame_coefficient_lambda * trace(E) * identity;

        std::vector<Tensor<1,dim> > phi_i_u (dofs_per_cell);
        std::vector<Tensor<2,dim> > phi_i_grads_u(dofs_per_cell);
        std::vector<Tensor<2,dim> > E_LinU(dofs_per_cell);
        std::vector<Tensor<2,dim> > sigma_phi(dofs_per_cell);

        for (unsigned int k=0; k<dofs_per_cell; ++k)
        {
            phi_i_u[k]       = fe_values[displacements].value (k, q);
            phi_i_grads_u[k] = fe_values[displacements].gradient (k, q);
            E_LinU[k] = 0.5 * (phi_i_grads_u[k] + transpose(phi_i_grads_u[k]));
            sigma_phi[k] = 2.0 * lame_coefficient_mu * E_LinU[k] +
                        lame_coefficient_lambda * trace(E_LinU[k]) * identity;
        }

        if (testcase == TestCase::benchmark)
            for (unsigned int i=0;i<dofs_per_cell;++i)
                local_rhs(i) -= dx *
                     ( (u - boundary_value[q]) * penalty * phi_i_u[i]
                     - (sigma_u * n) * phi_i_u[i]
                     + theta_DG * (sigma_phi[i] * n) * (u - boundary_value[q])
                     );

        else if (testcase == TestCase::cook)
        {
            if (boundary_color == 0)
                for (unsigned int i=0;i<dofs_per_cell;++i)
                    local_rhs(i) -= dx *
                         ( (u - boundary_value[q]) * penalty * phi_i_u[i]
                         - (sigma_u * n) * phi_i_u[i]
                         + theta_DG * (sigma_phi[i] * n) * (u - boundary_value[q])
                         );

            else if (boundary_color == 1)
                for (unsigned int i=0;i<dofs_per_cell;++i)
                    local_rhs(i) -= dx *
                         ( -neumann_value[q] * phi_i_u[i] );
        }
    } // end q_point
}

///////////////////////////////////////////////////////////////
template <int dim>
void DGElasticityPhaseFieldEquations<dim>::assemble_face_term_matrix(const FEFaceValuesBase<dim>& fe_values,
        const FEFaceValuesBase<dim>& fe_values_neighbor,
        const double face_measure,
        FullMatrix<double> &ui_phi_i_matrix,
        FullMatrix<double> &ue_phi_i_matrix,
        FullMatrix<double> &ui_phi_e_matrix,
        FullMatrix<double> &ue_phi_e_matrix) const
{
    const FEValuesExtractors::Vector displacements (0);
    const unsigned int   dofs_per_cell   = fe_values.dofs_per_cell;
    const unsigned int n_face_q_points   = fe_values.n_quadrature_points;

    Tensor<2,dim> identity = Tensors::get_Identity<dim>();

    for (unsigned int q=0; q<n_face_q_points; ++q)
    {
        const double dx =  fe_values.JxW(q);
        const Point<dim>& n = fe_values.normal_vector(q);
        double penalty = (penalty_u_face*degree*degree)/face_measure;

        std::vector<Tensor<1,dim>> phi_i_u(dofs_per_cell);
        std::vector<Tensor<1,dim>> phi_e_u(dofs_per_cell);
        std::vector<Tensor<2,dim>> sigma_phi_i(dofs_per_cell);
        std::vector<Tensor<2,dim>> sigma_phi_e(dofs_per_cell);

        for (unsigned int k=0; k<dofs_per_cell; ++k)
        {
            phi_i_u[k] = fe_values[displacements].value(k,q);
            phi_e_u[k] = fe_values_neighbor[displacements].value(k,q);

            Tensor<2,dim> phi_i_grad_u = fe_values[displacements].gradient(k,q);
            Tensor<2,dim> phi_e_grad_u = fe_values_neighbor[displacements].gradient(k,q);
            Tensor<2,dim> E_i_phi = 0.5 * (phi_i_grad_u + transpose(phi_i_grad_u));
            Tensor<2,dim> E_e_phi = 0.5 * (phi_e_grad_u + transpose(phi_e_grad_u));

            sigma_phi_i[k] = 2.0 * lame_coefficient_mu * E_i_phi +
                lame_coefficient_lambda * trace(E_i_phi) * identity;

            sigma_phi_e[k] = 2.0 * lame_coefficient_mu * E_e_phi +
                lame_coefficient_lambda * trace(E_e_phi) * identity;
        }

        for (unsigned i=0;i<dofs_per_cell;++i)
            for (unsigned j=0;j<dofs_per_cell;++j)
            {
                ui_phi_i_matrix(i,j) += dx * (
                        -0.5 * (sigma_phi_i[j] * n) * phi_i_u[i]
                        +0.5 * theta_DG * (sigma_phi_i[i] * n) * phi_i_u[j]
                        + penalty * phi_i_u[j] * phi_i_u[i] );

                ue_phi_i_matrix(i,j) += dx * (
                        -0.5 * (sigma_phi_e[j] * n) * phi_i_u[i]
                        -0.5 * theta_DG * (sigma_phi_i[i] * n) * phi_e_u[j]
                        - penalty * phi_e_u[j] * phi_i_u[i] );

                ui_phi_e_matrix(i,j) += dx * (
                        +0.5 * (sigma_phi_i[j] * n) * phi_e_u[i]
                        +0.5 * theta_DG * (sigma_phi_e[i] * n) * phi_i_u[j]
                        - penalty * phi_i_u[j] * phi_e_u[i] );

                ue_phi_e_matrix(i,j) += dx * (
                        +0.5 * (sigma_phi_e[j] * n) * phi_e_u[i]
                        -0.5 * theta_DG * (sigma_phi_e[i] * n) * phi_e_u[j]
                        + penalty * phi_e_u[j] * phi_e_u[i] );
            } // end j
    } // end q_point
}

///////////////////////////////////////////////////////////////
template <int dim>
void DGElasticityPhaseFieldEquations<dim>::assemble_face_term_rhs(
        const FEFaceValuesBase<dim>& fe_values,
        const FEFaceValuesBase<dim>& fe_values_neighbor,
        const double face_measure,
        Vector<double> &ui_phi_i_rhs,
        Vector<double> &ue_phi_i_rhs,
        Vector<double> &ui_phi_e_rhs,
        Vector<double> &ue_phi_e_rhs) const
{
    const FEValuesExtractors::Vector displacements (0);
    const unsigned int   dofs_per_cell   = fe_values.dofs_per_cell;
    const unsigned int n_face_q_points   = fe_values.n_quadrature_points;

    std::vector<Vector<double> > old_solution_values (n_face_q_points,
            Vector<double>(dim));

    std::vector<std::vector<Tensor<1,dim> > > old_solution_grads (n_face_q_points,
            std::vector<Tensor<1,dim> > (dim));

    std::vector<Vector<double> > old_solution_values_neighbor (n_face_q_points,
            Vector<double>(dim));

    std::vector<std::vector<Tensor<1,dim> > > old_solution_grads_neighbor (n_face_q_points,
            std::vector<Tensor<1,dim> > (dim));

    fe_values.get_function_values (solution, old_solution_values);
    fe_values.get_function_grads (solution, old_solution_grads);
    fe_values_neighbor.get_function_values (solution, old_solution_values_neighbor);
    fe_values_neighbor.get_function_grads (solution, old_solution_grads_neighbor);

    Tensor<2,dim> identity = Tensors::get_Identity<dim>();

    for (unsigned int q=0; q<n_face_q_points; ++q)
    {
        const double dx =  fe_values.JxW(q);
        const Point<dim>& n = fe_values.normal_vector(q);
        double penalty = (penalty_u_face*degree*degree)/face_measure;

        Tensor<1,dim> ui = Tensors::get_u<dim>(q, old_solution_values);
        Tensor<2,dim> grad_ui = Tensors::get_grad_u<dim>(q, old_solution_grads);
        Tensor<1,dim> ue = Tensors::get_u<dim>(q, old_solution_values_neighbor);
        Tensor<2,dim> grad_ue = Tensors::get_grad_u<dim>(q,old_solution_grads_neighbor);
        Tensor<2,dim> E_ui = 0.5 * (grad_ui + transpose(grad_ui));
        Tensor<2,dim> E_ue = 0.5 * (grad_ue + transpose(grad_ue));
        Tensor<2,dim> sigma_ui = 2.0 * lame_coefficient_mu * E_ui + lame_coefficient_lambda * trace(E_ui) * identity;
        Tensor<2,dim> sigma_ue = 2.0 * lame_coefficient_mu * E_ue + lame_coefficient_lambda * trace(E_ue) * identity;
        
        std::vector<Tensor<1,dim>> phi_i_u(dofs_per_cell);
        std::vector<Tensor<1,dim>> phi_e_u(dofs_per_cell);
        std::vector<Tensor<2,dim>> sigma_phi_i(dofs_per_cell);
        std::vector<Tensor<2,dim>> sigma_phi_e(dofs_per_cell);
        

        for (unsigned int k=0; k<dofs_per_cell; ++k)
        {
            phi_i_u[k] = fe_values[displacements].value(k,q);
            phi_e_u[k] = fe_values_neighbor[displacements].value(k,q);

            Tensor<2,dim> phi_i_grad_u = fe_values[displacements].gradient(k,q);
            Tensor<2,dim> E_pi = 0.5 * (phi_i_grad_u + transpose(phi_i_grad_u));
            Tensor<2,dim> phi_e_grad_u = fe_values_neighbor[displacements].gradient(k,q);
            Tensor<2,dim> E_pe = 0.5 * (phi_e_grad_u + transpose(phi_e_grad_u));

            sigma_phi_i[k] = 2.0 * lame_coefficient_mu * E_pi + lame_coefficient_lambda * trace(E_pi) * identity;
            sigma_phi_e[k] = 2.0 * lame_coefficient_mu * E_pe + lame_coefficient_lambda * trace(E_pe) * identity;
        }

        for (unsigned i=0;i<dofs_per_cell;++i)
        {
            ui_phi_i_rhs(i) -= dx * (
                    -0.5 * (sigma_ui * n) * phi_i_u[i]
                    +0.5 * theta_DG * (sigma_phi_i[i] * n) * ui
                    +penalty * ui * phi_i_u[i]);

            ue_phi_i_rhs(i) -= dx * (
                    -0.5 * (sigma_ue * n) * phi_i_u[i]
                    -0.5 * theta_DG * (sigma_phi_i[i] * n) * ue
                    -penalty * ue * phi_i_u[i]);

            ui_phi_e_rhs(i) -= dx * (
                    +0.5 * (sigma_ui * n) * phi_e_u[i]
                    +0.5 * theta_DG * (sigma_phi_e[i] * n) * ui
                    -penalty * ui * phi_e_u[i]);

            ue_phi_e_rhs(i) -= dx * (
                    +0.5 * (sigma_ue * n) * phi_e_u[i]
                    -0.5 * theta_DG * (sigma_phi_e[i] * n) * ue
                    +penalty * ue * phi_e_u[i]);
        } // end i
    } // end q_point
}



//-----------------------------------------------------------------------------
template <int dim>
class DGMethod
{
    public:
        DGMethod (InputHandler &parser);
        ~DGMethod ();

        void run ();

    private:
        void make_grid();
        void set_runtime_parameters ();
        void setup_system ();
        void assemble_system_matrix ();
        void assemble_system_rhs ();
        double newton_iteration();

        void solve ();
        double l2_error ();
        void h1_error (const double L2_Error);
        void check_local_equilibrium();        
        void stress_error();
        void output_results (const unsigned int cycle,
                const BlockVector<double> output_vector) const;

        class Postprocessor;
        
        void calculate_functionals();

        Triangulation<dim>   triangulation;
        const MappingQ1<dim> mapping;

        const unsigned int   degree;

        FESystem<dim>          fe;
        DoFHandler<dim>      dof_handler;

        ConstraintMatrix     constraints;

        BlockSparsityPattern      sparsity_pattern;
        BlockSparseMatrix<double> system_matrix;

        const QGauss<dim>   quadrature;
        const QGauss<dim-1> face_quadrature; 

        BlockVector<double>       solution;
        BlockVector<double>       system_rhs, newton_update;

        InputHandler &parser;
        TestCase testcase;
        DGElasticityPhaseFieldEquations<dim> dg;

        double tolerance_newton;
        double min_cell_diameter;
};

template <int dim>
DGMethod<dim>::
DGMethod(InputHandler &parser):mapping (),
    degree(1),
    fe (FE_DGQ<dim>(degree), dim),
    dof_handler (triangulation),
    quadrature (degree+2),
    face_quadrature (degree+2),
    parser(parser),
    testcase(TestCase::benchmark),
    dg (parser, degree)
{}

template <int dim>
DGMethod<dim>::~DGMethod ()
{
    dof_handler.clear ();
}

template<int dim>
void DGMethod<dim>::make_grid()
{
    std::string grid_name;

    if (testcase == TestCase::benchmark)
        grid_name  = "bench.inp";
    else if (testcase == TestCase::cook)
        grid_name  = "cook_alt.inp";
    else
        abort();

    GridIn<dim> grid_in;
    grid_in.attach_triangulation (triangulation);
    std::ifstream input_file(grid_name.c_str());
    Assert (dim==2, ExcInternalError());
    grid_in.read_ucd (input_file);
    triangulation.refine_global (parser.get_globalrefines());
}

template <int dim>
void DGMethod<dim>::set_runtime_parameters ()
{
    // Set test case
    if (parser.get_testcase() == "benchmark")
        testcase = TestCase::benchmark;
    else if (parser.get_testcase() == "cook")
        testcase = TestCase::cook;
    else
        testcase = TestCase::none;

    tolerance_newton = parser.get_tolerancenewton();
    make_grid();
}

template <int dim>
void DGMethod<dim>::setup_system ()
{
    system_matrix.clear ();
    dof_handler.distribute_dofs (fe);
    DoFRenumbering::Cuthill_McKee (dof_handler);
    std::vector<unsigned int> block_component (dim,0);
    block_component[1] = 1;     // y DoFs
    DoFRenumbering::component_wise (dof_handler, block_component);

    constraints.clear();
    constraints.close ();

    std::vector<unsigned int> dofs_per_block (2);
    DoFTools::count_dofs_per_block (dof_handler, dofs_per_block, block_component);
    const unsigned int n_u = dofs_per_block[0],
    n_c = dofs_per_block[1];

    std::cout << "Cells:\t"
        << triangulation.n_active_cells()
        << std::endl
        << "DoFs:\t"
        << dof_handler.n_dofs()
        << " (" << n_u << '+' << n_c << ')'
        << std::endl;

    BlockCompressedSimpleSparsityPattern csp (2,2);

    csp.block(0,0).reinit (n_u, n_u);
    csp.block(0,1).reinit (n_u, n_c);

    csp.block(1,0).reinit (n_c, n_u);
    csp.block(1,1).reinit (n_c, n_c);

    csp.collect_sizes();

    DoFTools::make_flux_sparsity_pattern (dof_handler, csp, constraints, false);

    sparsity_pattern.copy_from (csp);
    system_matrix.reinit (sparsity_pattern);

    // Actual solution at time step n
    solution.reinit (2);
    solution.block(0).reinit (n_u);
    solution.block(1).reinit (n_c);

    solution.collect_sizes ();
    solution = 0;

    // Updates for Newton's method
    newton_update.reinit (2);
    newton_update.block(0).reinit (n_u);
    newton_update.block(1).reinit (n_c);

    newton_update.collect_sizes ();
    newton_update = 0;

    // Residual for  Newton's method
    system_rhs.reinit (2);
    system_rhs.block(0).reinit (n_u);
    system_rhs.block(1).reinit (n_c);

    system_rhs.collect_sizes ();

    min_cell_diameter = 1.0e+10;
    double cell_diameter = 1.0e+9;
    typename DoFHandler<dim>::active_cell_iterator
        cell = dof_handler.begin_active(),
             endc = dof_handler.end();

    for (; cell!=endc; ++cell)
    {
        cell_diameter = cell->diameter();
        if (cell_diameter < min_cell_diameter)
            min_cell_diameter = cell_diameter;
    }

    std::cout << "Min cell dia: " << min_cell_diameter << std::endl;
    dg.set_min_cell_diameter(min_cell_diameter);
}


// Jacobian of Newton's method
template <int dim>
void DGMethod<dim>::assemble_system_matrix ()
{
    system_matrix=0;

    const unsigned int dofs_per_cell = dof_handler.get_fe().dofs_per_cell;
    std::vector<unsigned int> dofs (dofs_per_cell);
    std::vector<unsigned int> dofs_neighbor (dofs_per_cell);

    const UpdateFlags update_flags = update_values
        | update_gradients
        | update_quadrature_points
        | update_JxW_values;

    const UpdateFlags face_update_flags = update_values
        | update_gradients
        | update_quadrature_points
        | update_JxW_values
        | update_normal_vectors;

    const UpdateFlags neighbor_face_update_flags = update_values | update_gradients;

    FEValues<dim> fe_values (mapping, fe, quadrature, update_flags);
    FEFaceValues<dim> fe_values_face (mapping, fe, face_quadrature, face_update_flags);
    FESubfaceValues<dim> fe_values_subface (mapping, fe, face_quadrature, face_update_flags);
    FEFaceValues<dim> fe_values_face_neighbor (mapping, fe, face_quadrature, neighbor_face_update_flags);

    FullMatrix<double> ui_phi_i_matrix (dofs_per_cell, dofs_per_cell);
    FullMatrix<double> ue_phi_i_matrix (dofs_per_cell, dofs_per_cell);

    FullMatrix<double> ui_phi_e_matrix (dofs_per_cell, dofs_per_cell);
    FullMatrix<double> ue_phi_e_matrix (dofs_per_cell, dofs_per_cell);

    Vector<double>  local_rhs (dofs_per_cell);

    dg.get_solution_data (solution);

    typename DoFHandler<dim>::active_cell_iterator
        cell = dof_handler.begin_active(),
             endc = dof_handler.end();

    for (;cell!=endc; ++cell)
    {
        ui_phi_i_matrix = 0;
        fe_values.reinit (cell);

        dg.assemble_cell_term_matrix(fe_values,
                cell->diameter(),
                ui_phi_i_matrix);

        cell->get_dof_indices (dofs);

        for (unsigned int face_no=0; face_no<GeometryInfo<dim>::faces_per_cell; ++face_no)
        {
            typename DoFHandler<dim>::face_iterator face=
                cell->face(face_no);

            fe_values_face.reinit (cell, face_no);

            if (face->at_boundary())
            {
                unsigned int boundary_color = cell->face(face_no)->boundary_indicator();
                dg.assemble_boundary_term_matrix(fe_values_face,
                        face->measure(),
                        ui_phi_i_matrix,
                        boundary_color);
            }
            else
            {
                Assert (cell->neighbor(face_no).state() == IteratorState::valid,
                        ExcInternalError());
                typename DoFHandler<dim>::cell_iterator neighbor=
                    cell->neighbor(face_no);

                if (face->has_children())
                {
                    const unsigned int neighbor2=
                        cell->neighbor_face_no(face_no);

                    for (unsigned int subface_no=0;
                            subface_no<face->number_of_children(); ++subface_no)
                    {
                        typename DoFHandler<dim>::cell_iterator neighbor_child =
                            cell->neighbor_child_on_subface (face_no, subface_no);
                        Assert (!neighbor_child->has_children(), ExcInternalError());

                        ue_phi_i_matrix = 0;
                        ui_phi_e_matrix = 0;
                        ue_phi_e_matrix = 0;

                        fe_values_subface.reinit (cell, face_no, subface_no);
                        fe_values_face_neighbor.reinit (neighbor_child, neighbor2);

                        dg.assemble_face_term_matrix(fe_values_subface,
                                fe_values_face_neighbor,
                                neighbor_child->face(neighbor2)->measure(),
                                ui_phi_i_matrix,
                                ue_phi_i_matrix,
                                ui_phi_e_matrix,
                                ue_phi_e_matrix);

                        neighbor_child->get_dof_indices (dofs_neighbor);

                        for (unsigned int i=0; i<dofs_per_cell; ++i)
                            for (unsigned int j=0; j<dofs_per_cell; ++j)
                            {
                                system_matrix.add(dofs[i], dofs_neighbor[j],
                                        ue_phi_i_matrix(i,j));
                                system_matrix.add(dofs_neighbor[i], dofs[j],
                                        ui_phi_e_matrix(i,j));
                                system_matrix.add(dofs_neighbor[i], dofs_neighbor[j],
                                        ue_phi_e_matrix(i,j));
                            }
                    }
                }
                else
                {
                    if (!cell->neighbor_is_coarser(face_no) &&
                            (neighbor->index() > cell->index() ||
                             (neighbor->level() < cell->level() &&
                              neighbor->index() == cell->index())))
                    {
                        const unsigned int neighbor2=cell->neighbor_of_neighbor(face_no);
                        ue_phi_i_matrix = 0;
                        ui_phi_e_matrix = 0;
                        ue_phi_e_matrix = 0;

                        fe_values_face_neighbor.reinit (neighbor, neighbor2);

                        dg.assemble_face_term_matrix(fe_values_face,
                                fe_values_face_neighbor,
                                face->measure(),
                                ui_phi_i_matrix,
                                ue_phi_i_matrix,
                                ui_phi_e_matrix,
                                ue_phi_e_matrix);

                        neighbor->get_dof_indices (dofs_neighbor);

                        for (unsigned int i=0; i<dofs_per_cell; ++i)
                            for (unsigned int j=0; j<dofs_per_cell; ++j)
                            {
                                system_matrix.add(dofs[i], dofs_neighbor[j],
                                        ue_phi_i_matrix(i,j));
                                system_matrix.add(dofs_neighbor[i], dofs[j],
                                        ui_phi_e_matrix(i,j));
                                system_matrix.add(dofs_neighbor[i], dofs_neighbor[j],
                                        ue_phi_e_matrix(i,j));
                            }
                    }
                }
            }
        }
        constraints.distribute_local_to_global (ui_phi_i_matrix, dofs, system_matrix);
    }
}


// Right hand side residual of Newton's method
template <int dim>
void DGMethod<dim>::assemble_system_rhs ()
{
    system_rhs=0;

    const unsigned int dofs_per_cell = dof_handler.get_fe().dofs_per_cell;
    std::vector<unsigned int> dofs (dofs_per_cell);
    std::vector<unsigned int> dofs_neighbor (dofs_per_cell);

    const UpdateFlags update_flags = update_values
        | update_gradients
        | update_quadrature_points
        | update_JxW_values;

    const UpdateFlags face_update_flags = update_values
        | update_gradients
        | update_quadrature_points
        | update_JxW_values
        | update_normal_vectors;

    const UpdateFlags neighbor_face_update_flags = update_values | update_gradients;

    FEValues<dim> fe_values (mapping, fe, quadrature, update_flags);
    FEFaceValues<dim> fe_values_face (mapping, fe, face_quadrature, face_update_flags);
    FESubfaceValues<dim> fe_values_subface (mapping, fe, face_quadrature, face_update_flags);
    FEFaceValues<dim> fe_values_face_neighbor (mapping, fe, face_quadrature, neighbor_face_update_flags);

    Vector<double> ui_phi_i_rhs (dofs_per_cell);
    Vector<double> ue_phi_i_rhs (dofs_per_cell);
    Vector<double> ui_phi_e_rhs (dofs_per_cell);
    Vector<double> ue_phi_e_rhs (dofs_per_cell);

    dg.get_solution_data (solution);

    typename DoFHandler<dim>::active_cell_iterator
        cell = dof_handler.begin_active(),
        endc = dof_handler.end();

    for (;cell!=endc; ++cell)
    {
        ui_phi_i_rhs = 0;
        fe_values.reinit (cell);

        // Assemble element contributions
        dg.assemble_cell_term_rhs(fe_values,
                cell->diameter(),
                ui_phi_i_rhs);

        cell->get_dof_indices (dofs);

        for (unsigned int face_no=0; face_no<GeometryInfo<dim>::faces_per_cell; ++face_no)
        {
            typename DoFHandler<dim>::face_iterator face=
                cell->face(face_no);

            fe_values_face.reinit (cell, face_no);

            if (face->at_boundary())
            {
                unsigned int boundary_color = cell->face(face_no)->boundary_indicator();
                // Assemble boundary contributions
                dg.assemble_boundary_term_rhs(fe_values_face,
                        face->measure(),
                        ui_phi_i_rhs,
                        boundary_color);
            }
            else
            {
                Assert (cell->neighbor(face_no).state() == IteratorState::valid,
                        ExcInternalError());
                typename DoFHandler<dim>::cell_iterator neighbor=
                    cell->neighbor(face_no);

                // case b) and d)
                // Assemble face contributions in the interior
                // Here we need cell and neighbor contributions;
                // namely, four contributions
                if (face->has_children())
                {
                    const unsigned int neighbor2=
                        cell->neighbor_face_no(face_no);

                    for (unsigned int subface_no=0;
                            subface_no<face->number_of_children(); ++subface_no)
                    {
                        typename DoFHandler<dim>::cell_iterator neighbor_child
                            = cell->neighbor_child_on_subface (face_no, subface_no);
                        Assert (!neighbor_child->has_children(), ExcInternalError());

                        ue_phi_i_rhs = 0;
                        ui_phi_e_rhs = 0;
                        ue_phi_e_rhs = 0;

                        fe_values_subface.reinit (cell, face_no, subface_no);
                        fe_values_face_neighbor.reinit (neighbor_child, neighbor2);

                        dg.assemble_face_term_rhs(fe_values_subface,
                                fe_values_face_neighbor,
                                neighbor_child->face(neighbor2)->measure(),
                                ui_phi_i_rhs,
                                ue_phi_i_rhs,
                                ui_phi_e_rhs,
                                ue_phi_e_rhs);

                        neighbor_child->get_dof_indices (dofs_neighbor);

                        for (unsigned int i=0; i<dofs_per_cell; ++i)
                        {
                            system_rhs(dofs[i])          += ue_phi_i_rhs(i);
                            system_rhs(dofs_neighbor[i]) += ui_phi_e_rhs(i);
                            system_rhs(dofs_neighbor[i]) += ue_phi_e_rhs(i);
                        }
                    }
                }
                else
                {
                    // Case c)
                    if (!cell->neighbor_is_coarser(face_no) &&
                            (neighbor->index() > cell->index() ||
                             (neighbor->level() < cell->level() &&
                              neighbor->index() == cell->index())))
                    {
                        const unsigned int neighbor2=cell->neighbor_of_neighbor(face_no);

                        ue_phi_i_rhs = 0;
                        ui_phi_e_rhs = 0;
                        ue_phi_e_rhs = 0;

                        fe_values_face.reinit (cell, face_no);
                        fe_values_face_neighbor.reinit (neighbor, neighbor2);

                        dg.assemble_face_term_rhs(fe_values_face,
                            fe_values_face_neighbor,
                            face->measure(),
                            ui_phi_i_rhs,
                            ue_phi_i_rhs,
                            ui_phi_e_rhs,
                            ue_phi_e_rhs);

                        neighbor->get_dof_indices (dofs_neighbor);
                        for (unsigned int i=0; i<dofs_per_cell; ++i)
                        {
                            system_rhs(dofs[i])          += ue_phi_i_rhs(i);
                            system_rhs(dofs_neighbor[i]) += ui_phi_e_rhs(i);
                            system_rhs(dofs_neighbor[i]) += ue_phi_e_rhs(i);
                        }
                    }
                }
            }
        }
        constraints.distribute_local_to_global (ui_phi_i_rhs, dofs, system_rhs);
    }
}


// Newton's method with simple line search backtracking
template <int dim>
double DGMethod<dim>::newton_iteration ()
{
    Timer timer_newton;
    const double lower_bound_newton_residuum = tolerance_newton;
    const unsigned int max_no_newton_steps  = parser.get_maxitersnewt();

    // Decision whether the system matrix should be build
    // at each Newton step
    const double nonlinear_rho = 0.1;

    // Line search parameters
    unsigned int line_search_step;
    const unsigned int  max_no_line_search_steps = 10;
    const double line_search_damping = 0.6;
    double new_newton_residuum;

    //assemble_system_matrix();
    assemble_system_rhs();
    //solve();

    double newton_residuum = system_rhs.linfty_norm();
    double old_newton_residuum= newton_residuum;
    unsigned int newton_step = 1;

    if (newton_residuum < lower_bound_newton_residuum)
    {
        std::cout << '\t'
            << std::scientific
            << newton_residuum
            << std::endl;
    }

    while (newton_residuum > lower_bound_newton_residuum &&
            newton_step < max_no_newton_steps)
    {
        timer_newton.start();
        old_newton_residuum = newton_residuum;

        assemble_system_rhs();
        newton_residuum = system_rhs.linfty_norm();

        if (newton_residuum < lower_bound_newton_residuum)
        {
            std::cout << '\t'
                << std::scientific << newton_residuum << std::endl;
            break;
        }

        if (newton_residuum/old_newton_residuum > nonlinear_rho)
            assemble_system_matrix ();

        // Solve Ax = b
        solve ();

        line_search_step = 0;
        for ( ;line_search_step < max_no_line_search_steps; ++line_search_step)
        {
            solution += newton_update;
            assemble_system_rhs ();
            new_newton_residuum = system_rhs.linfty_norm();

            if (new_newton_residuum < newton_residuum)
                break;
            else
                solution -= newton_update;

            if (line_search_step == max_no_line_search_steps-1)
                solution +=newton_update;

            newton_update *= line_search_damping;
        }

        timer_newton.stop();

        std::cout << std::setprecision(5) <<newton_step << '\t'
            << std::scientific << newton_residuum << '\t'
            << std::scientific << newton_residuum/old_newton_residuum  <<'\t' ;

        if (newton_residuum/old_newton_residuum > nonlinear_rho)
            std::cout << "r" << '\t' ;
        else
            std::cout << " " << '\t' ;

        std::cout << line_search_step  << '\t'
            << std::scientific << timer_newton ()
            << std::endl;

        // Updates
        timer_newton.reset();
        newton_step++;
    }

    return newton_residuum;
}


template <int dim>
void DGMethod<dim>::solve ()
{
    Vector<double> sol, rhs;
    sol = newton_update;
    rhs = system_rhs;

    SparseDirectUMFPACK A_direct;
    A_direct.factorize(system_matrix);
    A_direct.vmult(sol,rhs);
    newton_update = sol;

    constraints.distribute (newton_update);
}



//-----------------------------------------------------------------------------
// Data output for Enriched Galerkin simplified
template<int dim> 
class DGMethod<dim>::Postprocessor : public DataPostprocessor<dim>
{
public:
    Postprocessor(): DataPostprocessor<dim>() {}
    
    virtual ~Postprocessor() {}
    
    virtual void compute_derived_quantities_vector (
        const std::vector<Vector<double>> &uh,
        const std::vector<std::vector<Tensor<1,dim>>> &duh,
        const std::vector<std::vector<Tensor<2,dim>>> &dduh,
        const std::vector<Point<dim>> &normals,
        const std::vector<Point<dim>> &evaluation_points,
        std::vector<Vector<double>> &computed_quantities) const;
    
    virtual std::vector<std::string> get_names () const;

    virtual
    std::vector<DataComponentInterpretation::DataComponentInterpretation>
    get_data_component_interpretation () const;

    virtual UpdateFlags get_needed_update_flags () const;   
};

template<int dim>
std::vector<std::string>
DGMethod<dim>::Postprocessor::get_names() const {
    std::vector<std::string> solution_names(1, "U");
    solution_names.push_back("U");
    solution_names.push_back("Ux");
    solution_names.push_back("Uy");
    solution_names.push_back("Stress");
    return solution_names;
}

template <int dim>
std::vector<DataComponentInterpretation::DataComponentInterpretation>
DGMethod<dim>::Postprocessor::get_data_component_interpretation () const {
  std::vector<DataComponentInterpretation::DataComponentInterpretation>
  interpretation (dim,
      DataComponentInterpretation::component_is_part_of_vector);
  interpretation.push_back(DataComponentInterpretation::component_is_scalar);
  interpretation.push_back(DataComponentInterpretation::component_is_scalar);
  interpretation.push_back(DataComponentInterpretation::component_is_scalar);
  return interpretation;
}


template <int dim>
UpdateFlags
DGMethod<dim>::Postprocessor::get_needed_update_flags() const
{
  return update_values | update_gradients | update_quadrature_points;
}

template <int dim>
void DGMethod<dim>::Postprocessor::compute_derived_quantities_vector (
    const std::vector<Vector<double>> &uh,
    const std::vector<std::vector<Tensor<1,dim>>> &duh,
    const std::vector<std::vector<Tensor<2,dim>>> &/*dduh*/,
    const std::vector<Point<dim>> &/*normals*/,
    const std::vector<Point<dim>> &evaluation_points,
    std::vector<Vector<double>> &computed_quantities) const
{
  const unsigned int n_quadrature_points = uh.size();
  Assert (computed_quantities.size() == n_quadrature_points, ExcInternalError());
  Assert (evaluation_points.size() == n_quadrature_points, ExcInternalError());
  Assert (uh[0].size() == dim, ExcInternalError());
  
  lameconstants getval;

  double mu = getval.get_lame_mu(), lambda = getval.get_lame_lambda();
  Tensor<2,dim> identity = Tensors::get_Identity<dim>();
  for (unsigned int q=0; q<n_quadrature_points; ++q)
  {
    Tensor<1,dim> u = Tensors::get_u<dim>(q, uh);
    Tensor<2,dim> grad_u = Tensors::get_grad_u<dim>(q, duh);
    computed_quantities[q][0] = u[0];
    computed_quantities[q][1] = u[1];
    computed_quantities[q][2] = u[0];
    computed_quantities[q][3] = u[1];

    Tensor<2,dim> E_u = 0.5*(grad_u + transpose(grad_u));
    Tensor<2,dim> sigma_u = 2 * mu * E_u + trace(E_u) * lambda * identity;
    computed_quantities[q][4] = sigma_u.norm();
  }
}


template <int dim>
void DGMethod<dim>::output_results (const unsigned int cycle,
        const BlockVector<double> output_vector) const
{
    Postprocessor postprocessor;
    DataOut<dim> data_out;
    data_out.attach_dof_handler (dof_handler);
    data_out.add_data_vector (solution, postprocessor);
    data_out.build_patches ();

    //TODO: change filename basis
    std::string filename_basis;
    if (testcase==TestCase::benchmark)
      filename_basis  = "DG_benchmark_";
    else if (testcase == TestCase::cook)
      filename_basis  = "DG_cook_";


    std::ostringstream filename;
    filename << filename_basis
        << Utilities::int_to_string (cycle, 2)
        << ".vtu";

    std::ofstream output (filename.str().c_str());
    data_out.write_vtu (output);

    std::cout << "-------------------------------------"
              << "\nSolution written to " << filename.str()
              << "\n-------------------------------------"
              << std::endl;
}


// Now, we arrive at the function that is responsible
// to compute the line integrals for the surface load
// evaluation.
template <int dim>
void DGMethod<dim>::calculate_functionals()
{
    const QGauss<dim-1> face_quadrature_formula (degree+2);
    FEFaceValues<dim> fe_face_values (fe, face_quadrature_formula,
            update_values | update_gradients | update_normal_vectors |
            update_JxW_values);

    const unsigned int dofs_per_cell = fe.dofs_per_cell;
    const unsigned int n_face_q_points = face_quadrature_formula.size();

    std::vector<unsigned int> local_dof_indices (dofs_per_cell);
    std::vector<Vector<double>>  old_solution_values (n_face_q_points,
            Vector<double> (dim));

    std::vector<std::vector<Tensor<1,dim>>>
        old_solution_grads (n_face_q_points, std::vector<Tensor<1,dim> > (dim));

    Tensor<1,dim> drag_lift_value, u_side;
    drag_lift_value.clear();
    u_side.clear();
    
    double area_side = 0.0;

    Tensor<2,dim> identity = Tensors::get_Identity<dim> ();

    double lame_coefficient_mu = dg.lame_coefficient_mu_global();
    double lame_coefficient_lambda = dg.lame_coefficient_lambda_global();

    typename DoFHandler<dim>::active_cell_iterator
        cell = dof_handler.begin_active(),
             endc = dof_handler.end();

    for (; cell!=endc; ++cell)
    {
        for (unsigned int face=0; face<GeometryInfo<dim>::faces_per_cell; ++face)
            if (cell->face(face)->at_boundary() &&
                    cell->face(face)->boundary_indicator()==1)
            {
                fe_face_values.reinit (cell, face);
                fe_face_values.get_function_values (solution, old_solution_values);
                fe_face_values.get_function_grads (solution, old_solution_grads);

                for (unsigned int q=0; q<n_face_q_points; ++q)
                {
                    const double dx = fe_face_values.JxW(q);

                    Tensor<2,dim> grad_u = Tensors::get_grad_u<dim>(q, old_solution_grads);
                    Tensor<1,dim> u_bc = Tensors::get_u<dim>(q, old_solution_values);

                    Tensor<2,dim> E = 0.5 * (grad_u + transpose(grad_u));
                    Tensor<2,dim> sigma_u = 2.0 * lame_coefficient_mu * E +
                        lame_coefficient_lambda * trace(E) * identity;

                    drag_lift_value +=  sigma_u *
                        fe_face_values.normal_vector(q) * dx;

                    area_side += dx;

                    u_side += u_bc * dx;
                }
            }
    }

    Tensor<1,dim> u_avg = u_side/area_side;
    std::cout << "Ux(tip): " << u_avg[0] << "\tFx(1): " << drag_lift_value[0] << std::endl;
    std::cout << "Uy(tip): " << u_avg[1] << "\tFy(1): " << drag_lift_value[1] << std::endl;
}



template<int dim>
double DGMethod<dim>::l2_error()
{
    const UpdateFlags update_flags = update_values 
            | update_quadrature_points 
            | update_JxW_values;
    QGauss<dim> error_quadrature(degree+2);    // over-solve
    FEValues<dim> fe_values (fe, error_quadrature, update_flags);

    double residual = 0.0;
    typename DoFHandler<dim>::active_cell_iterator
    cell = dof_handler.begin_active(),
    endc = dof_handler.end();

    BenchmarkSolutionEG<dim> exact_solution;

    for (; cell!=endc; ++cell)
    {        
        fe_values.reinit(cell);
        const unsigned int   n_q_points = fe_values.n_quadrature_points;
        const std::vector<double> &JxW = fe_values.get_JxW_values ();
        std::vector<Vector<double>> old_solution_values (n_q_points, Vector<double>(dim));
        fe_values.get_function_values(solution, old_solution_values);

        for (unsigned int q=0; q<n_q_points; ++q)
        {
            Tensor<1,dim> calculated_value = Tensors::get_u<dim>(q, old_solution_values);
            Tensor<1,dim> exact_value = exact_solution.value(fe_values.quadrature_point(q));
            Tensor<1,dim> cell_difference = calculated_value - exact_value;
            residual += JxW[q] * cell_difference.norm_square();
        }
    }

    double L2_Error = std::sqrt(residual);
    std::cout << "Cell Diameter: " << dg.get_min_cell_diameter()
              << std::endl << "L2 Norm of Error: " << L2_Error
              << std::endl;

    return L2_Error;
}


template<int dim>
void DGMethod<dim>::h1_error(const double L2_Error)
{
    const UpdateFlags update_flags = update_gradients 
            | update_quadrature_points 
            | update_JxW_values;
    const UpdateFlags face_update_flags = update_values
            | update_quadrature_points
            | update_JxW_values;
    const UpdateFlags neighbor_face_update_flags = update_values;

    QGauss<dim> cell_quadrature(degree+2);
    QGauss<dim-1> face_quadrature(degree+2);

    FEValues<dim> fe_values (mapping, fe, cell_quadrature, update_flags);
    FEFaceValues<dim> fe_values_face (mapping, fe, face_quadrature, face_update_flags);
    FEFaceValues<dim> fe_values_face_neighbor (mapping, fe, face_quadrature, neighbor_face_update_flags);

    double cell_residual = 0.0, face_residual = 0.0;
    const double penalty_u_bc = parser.get_penaltyBC();
    const double penalty_u_face = parser.get_penaltyDG();
    typename DoFHandler<dim>::active_cell_iterator
    cell = dof_handler.begin_active(),
    endc = dof_handler.end();

    BenchmarkSolutionEG<dim> exact_solution;

    for (; cell!=endc; ++cell)
    {        
        fe_values.reinit(cell);
        const unsigned int   n_q_points = fe_values.n_quadrature_points;
        const std::vector<double> &JxW = fe_values.get_JxW_values ();
        std::vector<std::vector<Tensor<1,dim>>> old_solution_grads (n_q_points, 
            std::vector<Tensor<1,dim>> (dim));        
        fe_values.get_function_grads(solution, old_solution_grads);

        for (unsigned int q=0; q<n_q_points; ++q)
        {
            Tensor<2,dim> grad_u_calculated = Tensors::get_grad_u<dim>(q, old_solution_grads);
            Tensor<2,dim> grad_u_exact = exact_solution.gradient(fe_values.quadrature_point(q));
            Tensor<2,dim> grad_u_difference = grad_u_calculated - grad_u_exact;
            Tensor<2,dim> E_diff = 0.5 * (grad_u_difference + transpose(grad_u_difference));

            cell_residual += JxW[q] * E_diff.norm_square();  // returns the induced (frobenius) norm
            //cell_residual += JxW[q] * tensor_norm(E_diff); // returns largest eigenvalue of E_diff
        }

        for (unsigned int face_no=0; face_no<GeometryInfo<dim>::faces_per_cell; ++face_no)
        {
            typename DoFHandler<dim>::face_iterator face = cell->face(face_no);
            fe_values_face.reinit(cell, face_no);
            
            const unsigned int n_face_q_points = fe_values_face.n_quadrature_points;
            const std::vector<double> &JxW_face = fe_values_face.get_JxW_values ();
            std::vector<Vector<double>> old_solution_values(n_face_q_points, Vector<double>(dim));
            std::vector<Vector<double>> old_solution_values_neighbor(n_face_q_points, Vector<double>(dim));

            if (face->at_boundary())
            {
                fe_values_face.get_function_values(solution, old_solution_values);
                const double penalty = (penalty_u_bc*degree*degree)/face->measure();
                BenchmarkDirichletBC<dim> dbc;

                for (unsigned int q=0; q<n_face_q_points; ++q)
                {
                    Tensor<1,dim> u_i = Tensors::get_u<dim>(q, old_solution_values);
                    Tensor<1,dim> u_d = dbc.value(fe_values_face.quadrature_point(q));
                    Tensor<1,dim> u_diff = u_i - u_d;
                    face_residual += JxW_face[q] * penalty * u_diff.norm_square();
                }
            }

            else
            {
                Assert (cell->neighbor(face_no).state() == IteratorState::valid,
                      ExcInternalError());
                typename DoFHandler<dim>::cell_iterator neighbor=cell->neighbor(face_no);
                // NO support for mesh refinement
                if (!cell->neighbor_is_coarser(face_no) &&
                        (neighbor->index() > cell->index() ||
                         (neighbor->level() < cell->level() &&
                          neighbor->index() == cell->index())))
                {
                    const unsigned int neighbor_face_no = cell->neighbor_of_neighbor(face_no);
                    fe_values_face_neighbor.reinit(neighbor, neighbor_face_no);

                    fe_values_face.get_function_values(solution, old_solution_values);
                    fe_values_face_neighbor.get_function_values(solution, old_solution_values_neighbor);

                    const double penalty = (penalty_u_face*degree*degree)/face->measure();

                    for (unsigned int q=0; q<n_face_q_points; ++q)
                    {
                        Tensor<1,dim> u_i = Tensors::get_u<dim>(q, old_solution_values);
                        Tensor<1,dim> u_e = Tensors::get_u<dim>(q, old_solution_values_neighbor);
                        Tensor<1,dim> u_diff = u_i - u_e;

                        face_residual += JxW_face[q] * penalty * u_diff.norm_square();
                    }
                }
            }
        }
    }

    double H1_error = std::sqrt(L2_Error*L2_Error + cell_residual);
    double elementwise_H1_error = std::sqrt(face_residual + cell_residual);
    std::cout << "Standard H1 Norm of Error: " << H1_error
              << std::endl << "Element-wise H1 Norm of Error: " << elementwise_H1_error
              << std::endl ;
}

template<int dim>
void DGMethod<dim>::check_local_equilibrium ()
{

  //Setup the new DoFHandler object that will manage the output
  DoFHandler<dim> output_dof_handler(triangulation);
  FESystem<dim> output_fe(FE_DGQ<dim>(0),2);
  output_dof_handler.distribute_dofs(output_fe);
  std::vector<unsigned int> block_component(2, 0);
  block_component[1] = 1;
  DoFRenumbering::component_wise(output_dof_handler, block_component);
  std::vector<unsigned int> dofs_per_block(2);
  DoFTools::count_dofs_per_block(output_dof_handler, dofs_per_block, block_component);
  const unsigned int n_x = dofs_per_block[0], n_y = dofs_per_block[1];
  BlockVector<double> output_vector;
  output_vector.reinit(2);
  output_vector.block(0).reinit(n_x);
  output_vector.block(1).reinit(n_y);
  output_vector.collect_sizes();
  output_vector = 0;

  //We are now read to perform the computation
  const UpdateFlags update_flags = update_quadrature_points
      | update_JxW_values;
  const UpdateFlags face_update_flags = update_values
      | update_gradients
      | update_quadrature_points
      | update_JxW_values
      | update_normal_vectors;
  const UpdateFlags neighbor_face_update_flags = update_values
      | update_gradients;
  FEValues<dim> fe_values (fe, quadrature, update_flags); 
  FEFaceValues<dim> fe_face_values (fe, face_quadrature, face_update_flags);
  FEFaceValues <dim> fe_neighbor_face_values (fe, face_quadrature, neighbor_face_update_flags);
  
  const unsigned int   n_q_points    = quadrature.size();
  const unsigned int   n_q_face_points = face_quadrature.size();    

  //Iterate with both DoF handlers
  typename DoFHandler<dim>::active_cell_iterator
  cell = dof_handler.begin_active(),
  endc = dof_handler.end();
  typename DoFHandler<dim>::active_cell_iterator
  output_cell = output_dof_handler.begin_active ();

  //Initialize vectors
  const unsigned int dofs_per_output_cell = output_dof_handler.get_fe().dofs_per_cell;
  const unsigned int dofs_per_solution_cell = dof_handler.get_fe().dofs_per_cell;
  std::vector<types::global_dof_index> solution_dofs (dofs_per_solution_cell);
  std::vector<types::global_dof_index> output_dofs (dofs_per_output_cell);
  std::vector<types::global_dof_index> output_neighbor_dofs (dofs_per_output_cell);

  //Only valid till we are only looking at mass balance (should be change as/if capabilities are expanded)
  Assert (dofs_per_output_cell == 2, ExcInternalError());

  double penalty_u_bc=parser.get_penaltyBC(), penalty_u_face=parser.get_penaltyDG();
  double lame_mu = dg.lame_coefficient_mu_global(), lame_lambda = dg.lame_coefficient_lambda_global();
  Tensor<2,dim> identity = Tensors::get_Identity<dim>();

  for (; cell!=endc; ++cell, ++output_cell)
  {
    fe_values.reinit(cell);
    output_cell->get_dof_indices (output_dofs);
    cell->get_dof_indices (solution_dofs);
    const std::vector<double> &JxW = fe_values.get_JxW_values ();
    const std::vector<Point<dim>> &qpoints = fe_values.get_quadrature_points();
    BenchmarkForcingFN<dim> body_force(lame_mu, lame_lambda);
    std::vector<Tensor<1,dim>> structure_force(n_q_points);
    body_force.value_list(qpoints, structure_force);

    Tensor<1,dim> cell_term;
    cell_term = 0;
    for (unsigned int q=0; q<n_q_points; ++q)
      cell_term += JxW[q] * structure_force[q];

    output_vector[output_dofs[0]] -= cell_term[0];
    output_vector[output_dofs[1]] -= cell_term[1];

    for (unsigned int face_no=0; face_no<GeometryInfo<dim>::faces_per_cell; ++face_no)
    {
      typename DoFHandler<dim>::face_iterator face = cell->face (face_no);
      
      fe_face_values.reinit (cell, face_no);
      std::vector<Vector<double> > old_solution_values (n_q_face_points,
              Vector<double>(dim));
      std::vector<std::vector<Tensor<1,dim> > > old_solution_grads (n_q_face_points,
              std::vector<Tensor<1,dim> > (dim));
      fe_face_values.get_function_values (solution, old_solution_values);
      fe_face_values.get_function_grads (solution, old_solution_grads);
      const std::vector<double> &face_JxW = fe_face_values.get_JxW_values ();
      const std::vector<Point<dim>> &face_qpoints = fe_face_values.get_quadrature_points();
      const std::vector<Point<dim>> &normals = fe_face_values.get_normal_vectors();
      /*--
       * Three Cases:
       * (a) Face is at the boundary - Apply D/N conditions
       * (b) Interface is non-matching
       * (c) Interface is matching
       */

      // Case (a) : Deal with boundary nodes
      if (face->at_boundary()) //--Case (a)
      {
        BenchmarkDirichletBC<dim> dbc;
        std::vector<Tensor<1,dim>> boundary_value(n_q_face_points);
        dbc.value_list(face_qpoints, boundary_value);
        Tensor<1,dim> boundary_term;
        boundary_term = 0;

        for (unsigned int q=0; q<n_q_face_points; ++q)
        {
          Tensor<1,dim> ui = Tensors::get_u<dim>(q, old_solution_values);
          Tensor<2,dim> grad_ui = Tensors::get_grad_u<dim>(q, old_solution_grads);
          Tensor<2,dim> E_i = 0.5 * (grad_ui + transpose(grad_ui));
          Tensor<2,dim> sigma_i = 2 * lame_mu * E_i + lame_lambda * trace(E_i) * identity;  
          boundary_term += face_JxW[q] * (
                        (-sigma_i * normals[q])
                        + ((penalty_u_bc*degree*degree)/face->measure()) * (ui - boundary_value[q])
                        );
        }
        output_vector[output_dofs[0]] += boundary_term[0];
        output_vector[output_dofs[1]] += boundary_term[1];
      }
      else
      {
        Assert (cell->neighbor (face_no).state () == IteratorState::valid,
            ExcInternalError ());
        typename DoFHandler<dim>::cell_iterator neighbor = cell->neighbor(face_no);
        Assert (output_cell->neighbor (face_no).state () == IteratorState::valid,
            ExcInternalError ());
        typename DoFHandler<dim>::cell_iterator output_neighbor = output_cell->neighbor(face_no);

        // Case (b): Deal with non-matching interfaces from the coarser side.
        if (face->has_children ()) //--Case (b)
        {
          std::cout << "No support for locally refined meshes!";
          abort();
        }
        // Case (c): Deal with matching interfaces from any ONE side (to avoid duplication).
        else if (!cell->neighbor_is_coarser(face_no)
          && (neighbor->index() > cell->index()
          || (neighbor->level() < cell->level()
              && neighbor->index() == cell->index()))) //--Case (c)
        {
          /*Note
           * The above condition ensures each matching interface is
           * touched only once. Hence once again both cells need to be operated upon.
           */

          const unsigned int neighbor_face = cell->neighbor_face_no (face_no);
          fe_neighbor_face_values.reinit (neighbor, neighbor_face);
          std::vector<Vector<double> > old_solution_values_neighbor (n_q_face_points,
                  Vector<double>(dim));
          std::vector<std::vector<Tensor<1,dim> > > old_solution_grads_neighbor (n_q_face_points,
                  std::vector<Tensor<1,dim> > (dim));
          fe_neighbor_face_values.get_function_values (solution, old_solution_values_neighbor);
          fe_neighbor_face_values.get_function_grads (solution, old_solution_grads_neighbor);

          Tensor<1,dim> interface_term;
          interface_term = 0.0;
          for (unsigned int q=0; q<n_q_face_points; ++q)
          {
            Tensor<1,dim> ui = Tensors::get_u<dim>(q, old_solution_values);
            Tensor<2,dim> grad_ui = Tensors::get_grad_u<dim>(q, old_solution_grads);
            Tensor<2,dim> E_i = 0.5 * (grad_ui + transpose(grad_ui));
            Tensor<2,dim> sigma_i = 2 * lame_mu * E_i + lame_lambda * trace(E_i) * identity;  
            Tensor<1,dim> ue = Tensors::get_u<dim>(q, old_solution_values_neighbor);
            Tensor<2,dim> grad_ue = Tensors::get_grad_u<dim>(q, old_solution_grads_neighbor);
            Tensor<2,dim> E_e = 0.5 * (grad_ue + transpose(grad_ue));
            Tensor<2,dim> sigma_e = 2 * lame_mu * E_e + lame_lambda * trace(E_e) * identity;

            interface_term += face_JxW[q] * (
                    -0.5 * ((sigma_i+sigma_e)*normals[q])
                    + ((penalty_u_face*degree*degree)/face->measure()) * (ui-ue)
                    );
          }
          output_vector[output_dofs[0]] += interface_term[0];
          output_vector[output_dofs[1]] += interface_term[1];
          output_neighbor->get_dof_indices (output_neighbor_dofs);
          output_vector[output_neighbor_dofs[0]] -= interface_term[0];
          output_vector[output_neighbor_dofs[1]] -= interface_term[1];
        } // end matching cell case
      } // end non-boundary face case
    } // end face loop
  } // end cell loop

  double l2_norm = 0;
  for (unsigned int k=0; k<output_vector.block(0).size(); ++k)
  {
    Tensor<1,dim> net_force;
    net_force[0] = output_vector.block(0)[k];
    net_force[1] = output_vector.block(1)[k];
    l2_norm += net_force.norm_square();
  }
  l2_norm = std::sqrt(l2_norm);

  std::cout << "L-inf norm of local equilibrium calculation: "
            << output_vector.linfty_norm() << std::endl;
  std::cout << "L2 norm of local equilibrium calculation: "
            << l2_norm << std::endl;

  output_dof_handler.clear();
}



template<int dim>
void DGMethod<dim>::stress_error ()
{

    const UpdateFlags update_flags = update_gradients 
            | update_quadrature_points 
            | update_JxW_values;
    const UpdateFlags face_update_flags = update_values
            | update_gradients
            | update_quadrature_points
            | update_JxW_values
            | update_normal_vectors;
    const UpdateFlags neighbor_face_update_flags = update_values
            | update_gradients;

    QGauss<dim> cell_quadrature(degree+2);
    QGauss<dim-1> face_quadrature(degree+2);

    FEValues<dim> fe_values (mapping, fe, cell_quadrature, update_flags);
    FEFaceValues<dim> fe_values_face (mapping, fe, face_quadrature, face_update_flags);
    FEFaceValues<dim> fe_values_face_neighbor (mapping, fe, face_quadrature, neighbor_face_update_flags);

    double cell_residual = 0.0, face_residual = 0.0;
    const double penalty_u_bc = parser.get_penaltyBC();
    const double penalty_u_face = parser.get_penaltyDG();
    typename DoFHandler<dim>::active_cell_iterator
    cell = dof_handler.begin_active(),
    endc = dof_handler.end();

    const double lame_mu = dg.lame_coefficient_mu_global(), lame_lambda = dg.lame_coefficient_lambda_global();
    const Tensor<2,dim> identity = Tensors::get_Identity<dim>();
    BenchmarkSolutionEG<dim> exact_solution;

    for (; cell!=endc; ++cell)
    {        
        fe_values.reinit(cell);
        const unsigned int   n_q_points = fe_values.n_quadrature_points;
        const std::vector<double> &JxW = fe_values.get_JxW_values ();
        std::vector<std::vector<Tensor<1,dim>>> old_solution_grads (n_q_points, 
            std::vector<Tensor<1,dim>> (dim));        
        fe_values.get_function_grads(solution, old_solution_grads);

        for (unsigned int q=0; q<n_q_points; ++q)
        {
            Tensor<2,dim> grad_u_calculated = Tensors::get_grad_u<dim>(q, old_solution_grads);
            Tensor<2,dim> grad_u_exact = exact_solution.gradient(fe_values.quadrature_point(q));
            Tensor<2,dim> grad_u_difference = grad_u_calculated - grad_u_exact;
            Tensor<2,dim> E_diff = 0.5 * (grad_u_difference + transpose(grad_u_difference));
            Tensor<2,dim> stress_diff = 2 * lame_mu * E_diff + lame_lambda * trace(E_diff) * identity;

            cell_residual += JxW[q] * stress_diff.norm_square();  // returns the induced (frobenius) norm
            //cell_residual += JxW[q] * tensor_norm(E_diff); // returns largest eigenvalue of E_diff
        }

        for (unsigned int face_no=0; face_no<GeometryInfo<dim>::faces_per_cell; ++face_no)
        {
            typename DoFHandler<dim>::face_iterator face = cell->face(face_no);
            fe_values_face.reinit(cell, face_no);
            
            const unsigned int n_face_q_points = fe_values_face.n_quadrature_points;
            const std::vector<double> &JxW_face = fe_values_face.get_JxW_values ();
            std::vector<Vector<double>> old_solution_face_values(n_face_q_points, Vector<double>(dim));
            std::vector<Vector<double>> old_solution_face_values_neighbor(n_face_q_points, Vector<double>(dim));
            std::vector<std::vector<Tensor<1,dim>>> old_solution_face_grads (n_face_q_points, 
                std::vector<Tensor<1,dim>> (dim));        
            std::vector<std::vector<Tensor<1,dim>>> old_solution_face_grads_neighbor (n_face_q_points, 
                std::vector<Tensor<1,dim>> (dim));        

            if (face->at_boundary())
            {
                fe_values_face.get_function_values(solution, old_solution_face_values);
                fe_values_face.get_function_grads(solution, old_solution_face_grads);
                const double penalty = (penalty_u_bc*degree*degree)/face->measure();
                BenchmarkDirichletBC<dim> dbc;

                for (unsigned int q=0; q<n_face_q_points; ++q)
                {
                    Point<dim> normal = fe_values_face.normal_vector(q);

                    Tensor<1,dim> u_i = Tensors::get_u<dim>(q, old_solution_face_values);
                    Tensor<1,dim> u_d = dbc.value(fe_values_face.quadrature_point(q));
                    Tensor<2,dim> grad_u_i = Tensors::get_grad_u<dim>(q, old_solution_face_grads);
                    Tensor<2,dim> E_i = 0.5 * (grad_u_i + transpose(grad_u_i));
                    Tensor<2,dim> sigma_i = 2 * lame_mu * E_i + lame_lambda * trace(E_i) * identity;
                    Tensor<1,dim> traction_i = -sigma_i * normal + penalty * (u_i - u_d);

                    Tensor<2,dim> grad_u_exact = exact_solution.gradient(fe_values_face.quadrature_point(q));
                    Tensor<2,dim> E_exact = 0.5 * (grad_u_exact + transpose(grad_u_exact));
                    Tensor<2,dim> sigma_exact = 2 * lame_mu * E_exact + lame_lambda * trace(E_exact) * identity;
                    Tensor<1,dim> traction_exact = -sigma_exact * normal;

                    Tensor<1,dim> diff = traction_i - traction_exact;
                    face_residual += JxW_face[q] * diff.norm_square();
                }
            }

            else
            {
                Assert (cell->neighbor(face_no).state() == IteratorState::valid,
                      ExcInternalError());
                typename DoFHandler<dim>::cell_iterator neighbor=cell->neighbor(face_no);
                // NO support for mesh refinement
                if (!cell->neighbor_is_coarser(face_no) &&
                        (neighbor->index() > cell->index() ||
                         (neighbor->level() < cell->level() &&
                          neighbor->index() == cell->index())))
                {
                    const unsigned int neighbor_face_no = cell->neighbor_of_neighbor(face_no);
                    fe_values_face_neighbor.reinit(neighbor, neighbor_face_no);

                    fe_values_face.get_function_values(solution, old_solution_face_values);
                    fe_values_face_neighbor.get_function_values(solution, old_solution_face_values_neighbor);
                    fe_values_face.get_function_grads(solution, old_solution_face_grads);
                    fe_values_face_neighbor.get_function_grads(solution, old_solution_face_grads_neighbor);

                    const double penalty = (penalty_u_face*degree*degree)/face->measure();

                    for (unsigned int q=0; q<n_face_q_points; ++q)
                    {
                        Point<dim> normal = fe_values_face.normal_vector(q);
                        
                        Tensor<1,dim> ui = Tensors::get_u<dim>(q, old_solution_face_values);
                        Tensor<2,dim> grad_ui = Tensors::get_grad_u<dim>(q, old_solution_face_grads);
                        Tensor<2,dim> E_i = 0.5 * (grad_ui + transpose(grad_ui));
                        Tensor<2,dim> sigma_i = 2 * lame_mu * E_i + lame_lambda * trace(E_i) * identity;  
                        Tensor<1,dim> ue = Tensors::get_u<dim>(q, old_solution_face_values_neighbor);
                        Tensor<2,dim> grad_ue = Tensors::get_grad_u<dim>(q, old_solution_face_grads_neighbor);
                        Tensor<2,dim> E_e = 0.5 * (grad_ue + transpose(grad_ue));
                        Tensor<2,dim> sigma_e = 2 * lame_mu * E_e + lame_lambda * trace(E_e) * identity;
                        Tensor<1,dim> traction_i = -0.5 * (sigma_i + sigma_e)*normal + penalty * (ui-ue); 

                        Tensor<2,dim> grad_u_exact = exact_solution.gradient(fe_values_face.quadrature_point(q));
                        Tensor<2,dim> E_exact = 0.5 * (grad_u_exact + transpose(grad_u_exact));
                        Tensor<2,dim> sigma_exact = 2 * lame_mu * E_exact + lame_lambda * trace(E_exact) * identity;
                        Tensor<1,dim> traction_exact = -sigma_exact * normal;

                        Tensor<1,dim> diff = traction_i - traction_exact;
                        face_residual += JxW_face[q] * diff.norm_square();
                    } // end q point
                } // end conditional
            } // end conditional
        } // end face iteration
    } // end cell iteration

    double l2_error = std::sqrt(cell_residual);
    double face_error = std::sqrt(face_residual);
    std::cout << "Interior Stress Error: " << l2_error
              << std::endl << "Face Stress Error: " << face_error
              << std::endl ;
}


template <int dim>
void DGMethod<dim>::run ()
{
    set_runtime_parameters ();
    setup_system ();

    {
        ConstraintMatrix constraints;
        constraints.close();

        //Initial guess for Newton Iteration
        VectorTools::project (dof_handler,
                constraints,
                QGauss<dim>(degree+2),
                ZeroFunction<dim>(dim),
                solution);
    }

    std::cout 
        << "\n====================================="
        << "\nParameters"
        << "\n====================================="
        << std::endl;

    std::cout 
        << "\n====================================="
        << "\nNewton Iteration Output"
        << "\n====================================="
        << std::endl;
    
    newton_iteration();

    std::cout 
        << "\n====================================="
        << "\nPostprocessing"
        << "\n====================================="
        << std::endl;    
    
    if (testcase == TestCase::benchmark)
    {
        h1_error(l2_error()); //H1 norm calculation needs L2 norm
        check_local_equilibrium();
        stress_error();
    }
    else if (testcase == TestCase::cook)
        calculate_functionals();
    
    output_results (parser.get_globalrefines(),solution);
    
    std::cout 
        << "\n-------------------------------------"
        << "\n-----------------END-----------------"
        << std::endl;    
}


int main (int argc, char **argv)
{

    InputHandler parser(argc, argv);

    lameconstants setval;
    setval.set_lame_mu(parser.get_lamemu());
    setval.set_lame_lambda(parser.get_lamelambda());

    try
    {
        using namespace dealii;
        const unsigned int dim = 2;
        {
            DGMethod<dim> dgmethod_iso(parser);
            dgmethod_iso.run ();
        }
    }
    catch (std::exception &exc)
    {
        std::cerr << std::endl << std::endl
            << "----------------------------------------------------"
            << std::endl;

        std::cerr << "Exception on processing: " << std::endl
            << exc.what() << std::endl
            << "Aborting!" << std::endl
            << "----------------------------------------------------"
            << std::endl;

        return 1;
    }
    catch (...)
    {
        std::cerr << std::endl << std::endl
            << "----------------------------------------------------"
            << std::endl;

        std::cerr << "Unknown exception!" << std::endl
            << "Aborting!" << std::endl
            << "----------------------------------------------------"
            << std::endl;

        return 1;
    };
    return 0;
}

// TODO
// - penalization parameter depends on time-dependent phase-field
// - DONE simple penalization -> might be improved by augmented Lagrangian
// - u as DG and phi as CG (maybe also phi in DG)
// - SIPG, NIPG, IIPG require careful testing and comparions
// - DONE make penalization parameter `h' or 'face-length' dependent
// - a hybrid DG/CG formulation in which DG is only solved `around' the crack
// - shear test: decomposition into tension and compression
