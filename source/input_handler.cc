#include "input_handler.h"

void InputHandler::print_usage_message() {
	std::cout <<"Please use the command line to specify "
			"an input parameter file with a -p flag\n"
			"You may use the following structure for your "
			"parameter file\n";

	prm.print_parameters (std::cout, ParameterHandler::Text);
}

void InputHandler::declare_parameters() {

	prm.enter_subsection("Elasticity Parameters"); {
		prm.declare_entry("Lame's Parameter Mu", "80.77e+3",
				Patterns::Double(0, -1),
				"Lame's Second Parameter a.k.a Shear Modulus [MPa]");
		prm.declare_entry("Lame's Parameter Lambda", "121.15e+3",
				Patterns::Double(0, -1),
				"Lame's First Parameter [MPa]"); 
	}
	prm.leave_subsection();

	prm.enter_subsection("DG Parameters"); {
		prm.declare_entry("Face Term Penalty", "1.0e+14",
				Patterns::Double(0, -1),
				"The penalty parameter for face terms in DG");
		prm.declare_entry("Boundary Term Penalty", "1.0e+10",
				Patterns::Double(0, -1),
				"The penalty parameter for enforcing Dirichlet boundary conditions in DG");
		prm.declare_entry("sform", "1",
				Patterns::Integer(-1,1),
				"The sform determines which class of DG method will be used"
				"\n(-1:SIPG, 0:IIPG, +1:NIPG, OBB)"
                "\nNote: SIPG is unstable and sensitive to changes in Penalty");
	}
	prm.leave_subsection();
	
    prm.enter_subsection("Runtime Parameters"); {
            prm.declare_entry("Maximum Newton Iterations", "50", 
					Patterns::Integer(0,-1),
					"Maximum allowable Newton Iterations at a given level");
            prm.declare_entry("Newton Tolerance", "5.0e-8",
                    Patterns::Double(0, -1),
                    "Tolerance of the newton iterations goes here");
            prm.declare_entry("Num Global Refines", "3",
                    Patterns::Integer(2,-1),
                    "Number of global refines prior to local refinement");
            prm.declare_entry("Test Case", "benchmark",
                    Patterns::Selection("benchmark|cook"),
                    "Name of the test case that is to be run");
	}
	prm.leave_subsection();
}

void InputHandler::parse_command_line(const int argc, char *const *argv) {
	if (argc <2) {
		print_usage_message ();
		exit (1);
	}

	std::list<std::string> args;
	for (int i=1; i<argc; ++i)
		args.push_back (argv[i]);

	while (args.size()) {
		if (args.front() == std::string("-p")) {
			if (args.size() == 1) {
				std::cerr << "Error: flag '-p' must be followed by the "
						<< "name of a parameter file."
						<< std::endl;
				print_usage_message ();
				exit (1);
			}
			args.pop_front ();
			const std::string parameter_file = args.front ();
			args.pop_front ();
			prm.read_input (parameter_file);

			prm.enter_subsection("Elasticity Parameters"); {
				lame_mu = prm.get_double("Lame's Parameter Mu");
				lame_lambda = prm.get_double("Lame's Parameter Lambda");
			}
			prm.leave_subsection();

			prm.enter_subsection("DG Parameters"); {
				penalty_DG = prm.get_double("Face Term Penalty");
				sform = prm.get_integer("sform");
				penalty_BC = prm.get_double("Boundary Term Penalty");
			}
			prm.leave_subsection();

			prm.enter_subsection("Runtime Parameters"); {
                testcase = prm.get("Test Case");
                global_refines = prm.get_integer("Num Global Refines");
                tolerance_newton = prm.get_double("Newton Tolerance");
                max_iters_newt = prm.get_integer("Maximum Newton Iterations");
			}
			prm.leave_subsection();

			flag_success = true;
		}
		else {
			args.pop_front ();
		}
	}

	if (flag_success == false) {
		std::cerr << "Error: Improperly specified input file." << std::endl;
		print_usage_message ();
		exit (1);
	}
}
